#include <stdio.h>
#include <stdlib.h>

int isSmallLetter(char elem);
int isBigLetter(char elem);
int isLetters(char str[255]);
char changeElement(char elem, int index);
int getElement( char str[255], int i);
int changeIndexElementBig(int ansiElement, int key);
int changeIndexElementSmall(int ansiElement, int key);

int main(int argc, char *argv[]){
   char str[255];
   //char garbage[255];
    if(argc < 2){
        printf("Wrong\n");
        return 1;
    } else {
        if(isLetters(argv[1]) == 0){
            printf("Wrong\n");
            return 1;
        } else {
            int i = 0, j = 0;
            //garbege = argv[1];
            //printf("plaintext: ");
            scanf("%[^\n]s", str);
            while(str[i] != '\0'){
                
                str[i] = changeElement(str[i], getElement(argv[1], j));
                i++;
                if(argv[1][j+1] == '\0'){
                    j = 0;
                } else if(isBigLetter(str[i]) != 0 || isSmallLetter(str[i]) != 0){
                    j++;
                }
                
            }
            printf("%s\n", str);
        }
    }
    
    return 0;
}

int isSmallLetter(char elem){
    if((int)elem > 96 && (int)elem < 123){
		return 1;
	} else {
		return 0;
	}
}

int isBigLetter(char elem){
    if((int)elem > 64 && (int)elem < 91){
		return 1;
	} else {
		return 0;
	}
}

int isLetters(char str[255]){
    int i = 0;
    while(str[i] != '\0'){
        if(isBigLetter(str[i]) == 0 && isSmallLetter(str[i]) == 0){
            return 0;
        }
        i++;
    }
    return 1;
}

int getElement( char str[255], int i){
    if(isBigLetter(str[i]) == 1){
        return (int)str[i] - 'A';
    } else {
        return (int)str[i] - 'a';
    }
}

int changeIndexElementSmall(int ansiElement, int key){
    ansiElement = ansiElement - 96 + key;
    if(ansiElement < 26){
        ansiElement += 96;
    } else {
        ansiElement = ansiElement + 96 - 26;
    }
    return ansiElement;
}

int changeIndexElementBig(int ansiElement, int key){
    ansiElement = ansiElement - 64 + key;
    if(ansiElement < 26){
        ansiElement += 64;
    } else {
        ansiElement = ansiElement + 64 - 26;
    }
    return ansiElement;
}

char changeElement(char elem, int key){
    while(key > 25){
        key = key - 26;
    }
    //printf("%i:%c\n", key, elem);
    if(isSmallLetter(elem) == 1){
        return (char)changeIndexElementSmall((int)elem, key);
    } else if(isBigLetter(elem) == 1){
        return (char)changeIndexElementBig((int)elem, key);
    } else {
        return elem;
    }
}