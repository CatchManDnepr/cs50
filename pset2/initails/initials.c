//initials
#include <stdio.h>
#include <stdlib.h>

void input();
void createNewString(char str[255]);
int isSmallLetter(char str);

int main(){
	input();
	return 0;
}

void input(){
	char num[255];
	scanf("%[^\n]s", num);
	/*ch = getchar();
		printf("Retry: ");
		scanf("%[^\n]s", num);
	}*/
	createNewString(num);
}

void createNewString(char str[255]){
	char newStr[255];
	int i = 0, newI = 0;
	//int ia = str[0];
	while(str[i] != '\0'){
		if((str[i-1] == ' ' && str[i] != ' ') || (i == 0 && str[i] != ' ')){
			if(isSmallLetter(str[i]) == 1){
				newStr[newI] = (char)((int)str[i] - 32);
			} else {
				newStr[newI] = str[i];
			}
			newI++;
		} 
		i++;
	}
	newStr[newI] = '\0';
	printf("%s\n", newStr);
}

int isSmallLetter(char str){
	if((int)str > 96 && (int)str < 123){
		return 1;
	} else {
		return 0;
	}
}
