//caesar
#include <stdio.h>
#include <stdlib.h>
//prototypes
int isSmallLetter(char elem);
int isBigLetter(char elem);
char changeElement(char elem, int key);
int chengeIndexElementSmall(int ansiElem, int key);
int chengeIndexElementBig(int ansiElem, int key);

int main(int argc, char *argv[]){
    char str[255];
    int i = 0;
    if(argc < 2 ){
        printf("Wrong\n");
        return 1;
    } else {
        scanf("%[^\n]s", str);
        while(str[i] != '\0'){
            if(argc == 2){
                str[i] = changeElement(str[i], atoi(argv[1]));
            } else {
                str[i] = changeElement(str[i], 0);
            }
            i++;
        }
        printf("%s\n", str);
    
    }
    
    return 0;
}
/*
function for check element on small letter
@params symbol
@return true(1)/false(0)
*/
int isSmallLetter(char elem){
    if((int)elem > 96 && (int)elem < 123){
		return 1;
	} else {
		return 0;
	}
}
/*
function for check element on big letter
@params symbol
@return true(1)/false(0)
*/
int isBigLetter(char elem){
    if((int)elem > 64 && (int)elem < 91){
		return 1;
	} else {
		return 0;
	}
}
/*
function for create encipher element(small letter) from input element
@params element from ansi table and key
@return new element
*/
int changeIndexElementSmall(int ansiElement, int key){
    ansiElement = ansiElement - 96 + key;
    if(ansiElement < 26){
        ansiElement += 96;
    } else {
        ansiElement = ansiElement + 96 - 26;
    }
    return ansiElement;
}
/*
function for create encipher element(big letter) from input element
@params element from ansi table and key
@return new element
*/
int changeIndexElementBig(int ansiElement, int key){
    ansiElement = ansiElement - 64 + key;
    if(ansiElement < 26){
        ansiElement += 64;
    } else {
        ansiElement = ansiElement + 64 - 26;
    }
    return ansiElement;
}
/*
function for create encipher element from input element
@params element and key
@return new element
*/
char changeElement(char elem, int key){
    while(key > 26){
        key = key - 26;
    }
    if(isSmallLetter(elem) == 1){
        return (char)changeIndexElementSmall((int)elem, key);
    } else if(isBigLetter(elem) == 1){
        return (char)changeIndexElementBig((int)elem, key);
    } else {
        return elem;
    }
}