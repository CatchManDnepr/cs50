//water
#include <stdio.h>
#include <stdlib.h>
//prorotypes
int input();
int check(char str[30]);
int isInt(char str[30]);

int main(){
	int min;
	printf("Input minutes: ");
	min = input();
	printf("%i\n", min*12);
	return 0;
}

/*
function for check data on integer
@param string
@return true(1)/false(0)
*/
int isInt(char str[30]){
	int i = 0;
	//int ia = str[0];
	while(str[i] != '\0'){
		if((int)str[i] < 48 || (int)str[i] > 57){
			return 0;
		}
		i++;
	}
	return 1;
}
/*
function for check exeption
@param string
@return true(1)/false(0)
*/
int check(char str[30]){
	int min;
	if(isInt(str) == 0){
		return 0;
	} else if(!atoi(str)){
		return 0;
	} else {
		min = atoi(str);
		if(min <= 0){
			return 0;
		}
	}
	return 1;
}

/*
function for input data
@params 
@return count minutes
*/
int input(){
	char num[30], ch;
	int min;
	scanf("%[^\n]s", num);
	while(check(num) == 0){
		ch = getchar();
		printf("Retry: ");
		scanf("%[^\n]s", num);
	}
	min = atoi(num);
	return min;
}



