#include <stdio.h>
#include <stdlib.h>
//prototypes
int input();
int check(char str[30]);
int isInt(char str[30]);
void createStairs(int n);
void outputMatrix(int n, char matrix[n][n]);

int main(){
	int stairs;
	printf("Input stairs: ");
	stairs = input();
	//char mario[stairs+1][stairs];
	//printf("%i", stairs);
	createStairs(stairs);
	return 0;
}
/*
function for check data on integer
@param string
@return true(1)/false(0)
*/
int isInt(char str[30]){
	int i = 0;
	//int ia = str[0];
	while(str[i] != '\0'){
		if((int)str[i] < 48 || (int)str[i] > 57){
			return 0;
		}
		i++;
	}
	return 1;
}
/*
function for check exeption
@param string
@return true(1)/false(0)
*/
int check(char str[30]){
	int num;
	if(isInt(str) == 0){
		return 0;
	} else if(!atoi(str)){
		if (str[0] != '0'){
			return 0;
		}
	} else {
		num = atoi(str);
		if(num < 0){
			return 0;
		} else if(num > 23){
			return 0;
		}
	}
	return 1;
}
/*
function for input data
@params 
@return count minutes
*/
int input(){
	char num[30], ch;
	int stairs;
	scanf("%[^\n]s", num);
	while(check(num) == 0){
		ch = getchar();
		printf("Retry: ");
		scanf("%[^\n]s", num);
	}
	stairs = atoi(num);
	return stairs;
}
/*
function for ctrate stairs
@params count stairs
@return massive of stairs
*/
void createStairs(int n){
    if(n > 0){
    	char mario[n][n];
    	for(int i = 0; i < n; i++){
			for(int j = 0; j< n; j++){
				if(n-j <= i + 1){
					mario[i][j] =  '#';
				} else {
					mario[i][j] =  ' ';
				}  
			}
		}
    	outputMatrix(n, mario);
    } 
}
/*
function for output stairs
@params count stairs, massive of stairs
@return output date on screen
*/
void outputMatrix(int n, char matrix[n][n]){
	//printf("%i", n);
    for(int i = 0; i < n; i++){
    	for(int j = 0; j<n; j++){
        	printf("%c", matrix[i][j]);
		}
		printf("  ");
		for(int j = n-1; j >=0; j--){
			if(n-j <= i + 1){
				printf("%c", matrix[i][j]);
			} else {
				break;
			}
		}
		printf("\n");
	}
}


