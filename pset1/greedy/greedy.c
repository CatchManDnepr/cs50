//greedy
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//protнpes
float input();
int check(char str[30]);
int isFloat(char str[30]);
void calkCount(float * countMoney, int * count, int coin);
int calk(int countMoney, int n, int *massCoins);

int main(){
	float num;
	int rezult;
	printf("Input count money: ");
	num = input();
	num *=100;
	//printf("%f\n", round(num));
	int mass[4] = {25, 10, 5, 1};
	rezult = calk(round(num), 4, mass);
	printf("%i\n", rezult);
	return 0;
}
/*
function for check data on float
@param string
@return true(1)/false(0)
*/
int isFloat(char str[30]){
	int i = 0;
	//int ia = str[0];
	while(str[i] != '\0'){
		if((int)str[i] < 48 || (int)str[i] > 57){
			if((int)str[i] != 46){
				return 0;
			}
		}
		i++;
	}
	return 1;
}
/*
function for check exeption
@param string
@return true(1)/false(0)
*/
int check(char str[30]){
	float num;
	if(isFloat(str) == 0){
		return 0;
	} else if(!atof(str)){
		return 0;
	} else {
		num = atof(str);
		if(num <= 0){
			return 0;
		}
	}
	return 1;
}
/*
function for input data
@params 
@return count minutes
*/
float input(){
	char num[30], ch;
	float money;
	scanf("%[^\n]s", num);
	while(check(num) == 0){
		ch = getchar();
		printf("Retry: ");
		scanf("%[^\n]s", num);
	}
	money = atof(num);
	return money;
}
/*
function for calk count coins
@params massive of kinds coins, count kinds coins and count money
@return count minutes
*/
int calk(int countMoney, int n, int massCoins[n]){
	int count = 0;
	float div;
	for(int i = 0; i < n; i++){
		div = countMoney/massCoins[i];
        if((int)div >= 1){
	       	count += (int)div;
	       	countMoney -= (int)div*massCoins[i];
	    }
	}
	return count;
}

