#include <stdio.h>
#include <stdlib.h>

int input();
int check(char str[30]);
int isInt(char str[30]);
void createStairs(int n);
void outputMatrix(int n, char matrix[n][n+1]);

int main(){
	int stairs;
	printf("Input stairs: ");
	stairs = input();
	//char mario[stairs+1][stairs];
	//printf("%i", stairs);
	createStairs(stairs);
	return 0;
}
/*
function for check data on integer
@param string
@return true(1)/false(0)
*/
int isInt(char str[30]){
	int i = 0;
	//int ia = str[0];
	while(str[i] != '\0'){
		if((int)str[i] < 48 || (int)str[i] > 57){
			return 0;
		}
		i++;
	}
	return 1;
}
/*
function for check exeption
@param string
@return true(1)/false(0)
*/
int check(char str[30]){
	int num;
	if(isInt(str) == 0){
		return 0;
	} else if(!atoi(str)){
		if (str[0] != '0'){
			return 0;
		}
	} else {
		num = atoi(str);
		if(num <= -1){
			return 0;
		} else if(num > 23){
			return 0;
		}
	}
	return 1;
}
/*
function for input data
@params 
@return count minutes
*/
int input(){
	char num[30], ch;
	int stairs;
	scanf("%[^\n]s", num);
	while(check(num) == 0){
		ch = getchar();
		printf("Retry: ");
		scanf("%[^\n]s", num);
	}
	stairs = atoi(num);
	return stairs;
}
/*
function for ctrate stairs
@params count stairs
@return massive of stairs
*/
void createStairs(int n){
    char mario[n][n+1];
    if(n > 0){
    	for(int i = 0; i < n; i++){
			for(int j = 0; j< n+1; j++){
				if(j + 2 >= n - i + 1){
					mario[i][j] =  '#';
				} else {
					mario[i][j] =  ' ';
				}  
			}
		}
    	outputMatrix(n, mario);
    } 
}
/*
function for output stairs
@params count stairs, massive of stairs
@return output date on screen
*/
void outputMatrix(int n, char matrix[n][n+1]){
	//printf("%i", n);
    for(int i = 0; i < n; i++){
    	for(int j = 0; j<n+1; j++){
        	printf("%c", matrix[i][j]);
		}
		printf("\n");
	}
}

