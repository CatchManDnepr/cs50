/**
 * helpers.c
 *
 * Helper functions for Problem Set 3.
 */
 
#include <cs50.h>

#include "helpers.h"

/**
 * Returns true if value is in array of n values, else false.
 */
bool search(int value, int values[], int n)
{
    bool index = true;
    int down = 0, top = n-1;
    while(down <= top && index){
        if(down == top){
            index = false;
        }
        if (values[(top+down)/2] < value){
            down = (top+down+1)/2;
        } else if (values[(top+down)/2] > value){
            top = (top+down+1)/2;
        } else {
            return true;
        }
    }
    return false;
}

/**
 * Sorts array of n values.
 */
void sort(int values[], int n)
{
    int tmp;
    for(int j = 1; j < n; j++){
        for (int i = 0; i < n - j; i++) {
            if (values[i] > values[i + 1]) {
                tmp = values[i];
                values[i] = values[i + 1];
                values[i + 1] = tmp;
            }
        }
     }
    /*int max = 0; 
    for(int i = 0; i < n; i++){
        if(max < values[i]){
            max = values[i];
        }
    }
    int newMass [max + 1];
    for(int i = 0; i <= max; i++){
        newMass[i] = 0;
    }
    for(int i = 0; i < n; i++){
        newMass[values[i]]++;
    }
    int z = 0;
    for(int i = 0; i < max+1; i++){
        while(newMass[i] > 0){
            values[z] = i;
            z++;
        }
    }*/
}